var server = require('http').createServer();
var io = require('socket.io')(server);
var port = 3000;
var allPlayers = [];
var allGames = {};
var keyLength = 6;

server.listen(port, function() {
	console.log('Starting server on *:' + port);

	initializeDebugData();
});

function initializeDebugData() {
	var g = new Game();
	var p1 = new Player(null);
	p1.username = 'TODD';
	p1.gameKey = '1';
	var p2 = new Player(null);
	p2.username = 'BOB';
	p2.gameKey = '1'
	g.addPlayer(p1);
	g.addPlayer(p2);
	allGames['game_1'] = g;
}

//Most of these asserts should be removed for production so the server doesn't crash all over the place!

io.on('connection', function(socket) {
	console.log(socket.id + ' connected');
	
	//Create a new player for the connected client, add it to the list of players, and notify client of connection
	var player = new Player(socket);
	allPlayers.push(player);
	socket.emit('connected', 'Admit It Dev. v0.1');

	
	/* Called when the player requests to create a new game */
	socket.on('create game', function(username, callback) {
		assert(!player.inGame(), player.uid + ' tried to create a game when already in a game');
		
		setPlayerUsername(player, username);
		createNewGame(player);

		callback('success');
	});

	/* Called when the player cancels their new game */
	socket.on('cancel create', function() {
		assert(player.inGame(), player.uid + ' tried to cancel a game he/she is not in');
		
		cancelGameCreation(player);
	});

	/* Called when the player starts the game after all other players have joined */
	socket.on('start game', function() {
		assert(player.inGame(), player.uid + ' tried to start a game he/she is not in');
		
		playerStartedGame(player);
	});

	/* Called when the player requests to join a game */
	socket.on('join game', function(username, gameKey, callback) {
		assert(!player.inGame(), player.uid + ' tried to join a game when already in a game');		
		
		//FOR DEBUG ONLY
		player.socket.join(gamePrefix(gameKey));
		var result = addPlayerToGame(player, username, gameKey);
		
		callback(result);
	});

	/* Called when the player requests a list of other players in their game as well as the game key */
	socket.on('get info', function(callback) {
		assert(player.inGame(), player.uid + ' tried to request users from a game he/she is not in');
		
		var game = getGame(player.gameKey);
		
		callback(player.gameKey, game.getPlayerUsernameArray(player.username));
	});

	/* Called when a player submits a new question to their game */
	socket.on('add question', function(questionText) {
		assert(player.inGame(), player.uid + ' tried to leave a game he/she was not in');
		
		playerAddedQuestion(player, questionText);
	});

	/* Called when questions shold no longer be collected and round 1 should start */
	socket.on('round ready', function() {
		assert(player.inGame(), player.uid + ' tried to start a game he/she is not in');

		var remaining = playerFinishedAddingQuestions(player);
		if (remaining <= 0) {
			startRound(player);
		}
	});

	/* Called when a player reqeusts to leave a game but still be connected to the server */
	socket.on('leave game', function() {
		assert(player.inGame(), player.uid + ' tried to leave a game he/she was not in');
		
		removePlayerFromGame(player, false);
	});

	/* Called when a client completely disconnects from the server */
	socket.on('disconnect', function() {
		playerDisconnected(player);
	});	
});

/* Events */
function setPlayerUsername(player, username) {
	player.username = username;
	
	console.log(player.uid + ' set username to ' + player.username);
}

function createNewGame(player) {
	var keyBase = player.uid + player.username;
	var gameKey = generateKeyNumerical(keyLength, keyBase);
	while (gamePrefix(gameKey) in allGames) {
		/* While this is unsafe, in practice, only time will tell if it truly is going to be an issue */
		gameKey = generateKeyNumerical(keyLength, keyBase);
	}

	var game = new Game();
	game.addPlayer(player);
	allGames[gamePrefix(gameKey)] = game;

	player.socket.join(gamePrefix(gameKey));
	player.gameKey = gameKey;
	
	console.log(player.uid + ' created new game: ' + gameKey);
}

function cancelGameCreation(player) {
	var game = getGame(player.gameKey);
	if (game != null && game.gameState == GameState.WAITING_FOR_PLAYERS) {
		io.sockets.in(gamePrefix(player.gameKey)).emit('game canceled');
		removeAllPlayersFromGame(game);

		console.log(player.uid + ' canceled their game');
	}
}

function playerStartedGame(player) {
	var game = getGame(player.gameKey);
	if (game != null && game.gameState == GameState.WAITING_FOR_PLAYERS) {
		game.beginQuestionCreation();
		io.sockets.in(gamePrefix(player.gameKey)).emit('game started');

		console.log(player.uid + ' started game ' + player.gameKey);
	}
}

function addPlayerToGame(player, username, gameKey) {
	var gameRoom = io.sockets.adapter.rooms[gamePrefix(gameKey)];
	if (gameRoom != undefined) {
		var game = getGame(gameKey);
		if (game != null && game.containsPlayer(username)) {
			return 'name';
		} else if (game != null && game.gameState == GameState.WAITING_FOR_PLAYERS) {
			setPlayerUsername(player, username);
			game.addPlayer(player);
			player.socket.join(gamePrefix(gameKey));
			player.gameKey = gameKey;
			io.sockets.in(gamePrefix(gameKey)).emit('player joined', player.username);
		
			console.log(player.uid + ' joined ' + player.gameKey);

			return 'success';
		}
	}

	return 'failed';
}

function removePlayerFromGame(player, expected) {
	var game = getGame(player.gameKey);
	if (game != null) {
		var key = player.gameKey;
		game.removePlayer(player);
		checkForEmptyGame(key);

		if (!expected) {
			io.sockets.in(gamePrefix(player.gameKey)).emit('player left', player.username);
		}

		player.socket.leave(gamePrefix(player.gameKey));
		player.gameKey = null;

		console.log(player.uid + ' left their game');
	}
}

function playerDisconnected(player) {
	var key = player.gameKey;
	removePlayerFromGame(player, false);
	checkForEmptyGame(key);
	allPlayers.splice(allPlayers.indexOf(player), 1);

	console.log(player.uid + ' disconnected');
}

function playerAddedQuestion(player, questionText) {
	var game = getGame(player.gameKey);
	if (game != null && game.gameState == GameState.WAITING_FOR_QUESTIONS) {
		var question = new Question(questionText);
		game.addNewQuestion(question);
		io.sockets.in(gamePrefix(player.gameKey)).emit('added question', question.text);

		console.log(player.uid + " added '" + question.text + "' to game " + player.gameKey);
	}
}

function playerFinishedAddingQuestions(player) {
	var game = getGame(player.gameKey);
	if (game != null && game.gameState == GameState.WAITING_FOR_QUESTIONS) {
		game.playerReady();

		console.log(player.uid + ' is ready');

		return game.unfinishedPlayers;
	}
}

function startRound(player) {
	var game = getGame(player.gameKey);
	if (game != null && game.gameState == GameState.WAITING_FOR_QUESTIONS) {
		game.beginFirstRound();
		io.sockets.in(gamePrefix(player.gameKey)).emit('round started');

		console.log('Game ' + player.gameKey + ' is starting a round');
	}
}

/* Event Helpers */
function getGame(gameKey) {
	if (gamePrefix(gameKey) in allGames) {
		return allGames[gamePrefix(gameKey)];
	}

	return null;
}

function removeAllPlayersFromGame(game) {
	if (game != null) {
		game.players.forEach(function(player, index) {
			removePlayerFromGame(player, true);
		});
	}
}

function checkForEmptyGame(gameKey) {
	if (gamePrefix(gameKey) in allGames && getGame(gameKey).isEmpty()) {
		deleteGame(gameKey);
	}
}

function deleteGame(gameKey) {
	delete allGames[gamePrefix(gameKey)];

	console.log('Game ' + gameKey + ' was deleted');
}

/* Structure */

//Game Class
var GameState = {
	WAITING_FOR_PLAYERS: 0,
	WAITING_FOR_QUESTIONS: 1,
	PLAYING_ROUND: 2
};

function Game() {

	//TODO: Store time created and expiration time based on current state
	//Periodically run through all games and delete expired ones

	this.gameState = GameState.WAITING_FOR_PLAYERS;
	this.players = [];
	this.questionBank = [];
	this.unfinishedPlayers = 0;
}

Game.prototype.addPlayer = function(player) {
	this.players.push(player);
};

Game.prototype.removePlayer = function(player) {
	var index = this.players.indexOf(player);
	if (index > -1) {
		this.players.splice(index, 1);
	}
};

Game.prototype.containsPlayer = function(username) {
	var result = false;
	this.players.forEach(function(otherPlayer, index) {
		if (otherPlayer.username == username) {
			result = true;
			return;
		}
	});

	return result;
};

Game.prototype.beginQuestionCreation = function() {
	this.gameState = GameState.WAITING_FOR_QUESTIONS;
	this.unfinishedPlayers = this.players.length;
	shuffle(this.players);
};

Game.prototype.playerReady = function() {
	this.unfinishedPlayers -= 1;
};

Game.prototype.beginFirstRound = function() {
	this.gameState = GameState.PLAYING_ROUND;
	shuffle(this.questionBank);
};

Game.prototype.playerCount = function() {
	return this.players.length;
};

Game.prototype.isEmpty = function() {
	return this.playerCount() == 0;
};

Game.prototype.addNewQuestion = function(question) {
	this.questionBank.push(question);
};

Game.prototype.getQuestionTextArray = function() {
	var qs = [];
	this.questionBank.forEach(function(question, index) {
		qs.push(question.text);
	});

	return qs;
};

Game.prototype.getPlayerUsernameArray = function(name) {
	var us = [];
	this.players.forEach(function(player, index) {
		var username = player.username;
		if (name == null || username != name) {
			us.push(username);
		}
	});

	return us;
};

//Player Class
function Player(soc) {
	this.uid = soc == null ? 'DEBUG' : soc.id;
	this.socket = soc;
	this.username = null;
	this.gameKey = null;
}

Player.prototype.hasUsername = function() {
	return this.username != null;
};

Player.prototype.inGame = function() {
	return this.gameKey != null;
};

//Question Class
function Question(questionText) {
	this.text = questionText;
	this.used = false;
	this.incentiveWorth = 0;
}

/* Utility Methods */
function generateKeyNumerical(length, uid) {
	var idn = '';
	for (var i = 0; i < uid.length; i++) {
		idn = idn + uid.charCodeAt(i);
	}

	var key = '';
	for (var i = 0; i < length; i++) {
		key = key + idn.charAt(randInt(0, idn.length));
	}

	return key;
}

function randInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function gamePrefix(gameKey) {
	return 'game_' + gameKey;
}

function listPlayers() {
	console.log('---- Players ----');
	allPlayers.forEach(function(player, index) {
		console.log(player.uid);
	});

	console.log('---- Players ----');
}

function shuffle(array) {
    var counter = array.length, temp, index;
    while (counter > 0) {
        index = Math.floor(Math.random() * counter);
        counter--;

        temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

function assert(condition, message) {
	if (!condition) {
		message = message || "Assertion Failed";
		if (typeof Error !== "undefined") {
			throw new Error(message);
		}

		throw message;
	}
}